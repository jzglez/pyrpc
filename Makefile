##############################################################################
# Makefile for libpyrpc.so
##############################################################################

SRCDIR := src
OBJDIR := obj
LIBDIR := lib

CFILES := $(wildcard $(SRCDIR)/*.c)
OBJFILES := $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(CFILES))
DEPFILES := $(OBJFILES:.o=.d)

TARGET := $(LIBDIR)/libpyrpc.so
INCLUDES := -I.
CFLAGS := $(INCLUDES) -Wall -Werror -Wextra
LDFLAGS := -shared

CFLAGS += -MMD -fPIC #-pedantic -Wall -Wextra -ggdb3

all: debug
debug: CFLAGS += -g
debug: objdir libdir $(TARGET)
release: CFLAGS += -O3
release: objdir libdir $(TARGET)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) -c $(CFLAGS) $< -o $@

$(TARGET): $(OBJFILES)
	$(LINK.c) $(LDFLAGS) $^ -o $@

objdir:
	mkdir -p $(OBJDIR)

libdir:
	mkdir -p $(LIBDIR)

clean:
	rm -f $(TARGET) $(OBJFILES) $(DEPFILES)

-include $(DEPFILES)

.PHONY: clean all debug release libdir objdir

