/******************************************************************************
 * get_last_shotno.h - Interface for get_last_shot
 *
 * Author: J. C. Gonzalez <jose.carlos.gonzalez@ipp.mpg.de>
 * Last update: 2023.10.13
 ******************************************************************************/

#ifndef GET_LAST_SHOTNO
#define GET_LAST_SHOTNO

#ifdef __cplusplus
extern "C" {
#endif

#include "rpcjson.h"

#define NOT_VALID_SHOTNO    (BAD_RESPONSE + 1)

void setRShotdServerHostPort(char * address, int port);    
int getLastShotno();
const char * getShotnoError();
    
#ifdef __cplusplus
}
#endif

#endif /* GET_LAST_SHOTNO */
