/******************************************************************************
 * diagsync_if.c - Implementation of the interface to DiagSync server
 *
 * This functons perform requests to the Python diagsync server
 *
 * Author: J. C. Gonzalez <jose.carlos.gonzalez@ipp.mpg.de>
 * Last update: 2023.10.13
 ******************************************************************************/

#include <stdio.h>              /* printf, sprintf */
#include <stdlib.h>             /* exit, malloc, free */
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>

#include "diagsync_if.h"

#define DSHOST    "localhost"
#define DSPORT    52016

static char serverDiagSyncHostName[128] = DSHOST;
static int  serverDiagSyncPortNumber    = DSPORT;

#define SKIP(x)  (void)(x)
#define MAX_REQUEST_SIZE  MAX_RESPONSE_SIZE

static bool debug = true;  /* set to true to get debug messages in stderr */

time_t getDatum()
{
    struct timeval  tp;
    struct timezone tzp;

    gettimeofday(&tp, &tzp);
    return tp.tv_sec;
}

void setDiagSyncServerHostPort(char * address, int port)
{
    /* Set host name and port number for server */
    strcpy(serverDiagSyncHostName, address);
    serverDiagSyncPortNumber = port;
}

int execRequest_getResponse(char * rqst, char * resp)
{
    char statusStr[6];
    int statusLen;
    char *spos = NULL, *spos1 = NULL;
    int result;
    
    memset(resp, 0, MAX_RESPONSE_SIZE);

    setServerAddress(serverDiagSyncHostName, serverDiagSyncPortNumber);

    result = execRPCjson(rqst, resp);
    if (result != NO_ERROR) { return result; }

    if (debug) fprintf(stderr, "Response:\n<<<%s>>>\n", resp);

    /* Process response */
    spos = strstr(resp, "\"response\": ");
    if (spos == NULL) return( -BAD_RESPONSE );
    spos1 = spos + 13;
    spos = strstr(spos1, ",");
    if (spos == NULL) return( -BAD_RESPONSE );
    statusLen = spos - spos1;
    strncpy(statusStr, spos1, statusLen);
    statusStr[statusLen] = 0;

    if (0 != strcmp(statusStr, "true")) { return EXIT_FAILURE; }

    return EXIT_SUCCESS;
}

bool getIntValueOf(char * key, char * str, int * value)
{
    char subStr[64];
    char * spos, * spos1;
    sprintf(subStr, "\"%s\": ", key);
    spos = strstr(str, subStr);
    if (spos == NULL) return false;
    spos1 = spos + strlen(subStr);
    *value = atoi(spos1);

    return true;
}

bool getStrValueOf(char * key, char * str, char * value)
{
    char subStr[64];
    char * spos, * spos1;
    int valueLen;
    sprintf(subStr, "\"%s\": ", key);
    spos = strstr(str, subStr);
    if (spos == NULL) return false;
    spos1 = spos + strlen(subStr);
    spos = strstr(spos1, ",");
    if (spos == NULL) {
	spos = strstr(spos1, "}");
	if (spos == NULL) return false;
    }
    valueLen = spos - spos1 - 1; /* remove quotes */
    strncpy(value, spos1 + 1, valueLen);
    return true;
}

/* SET */
int diagsyncprog_1 (char *host, char *diag, int typ, int status, uint32_t shotnr,
		            int datum, uint32_t uid, int pid)
{
    char body[MAX_REQUEST_SIZE];
    char response[MAX_RESPONSE_SIZE];
    int result;
    char bodyTpl[] =
        "{\"id\": 1, \"jsonrpc\": \"2.0\", \"method\": \"diagsyncRequest\", "
        "\"params\": [{\"__jsonclass__\": [\"ds_defs.DiagSyncRequest\", []], "
	    "\"cmd\": \"set\", \"data\": {\"diag\": \"%s\", \"typ\": %d, "
	    "\"status\": %d, \"shotnr\": %d, \"datum\": %lu, "
	    "\"uid\": %u, \"pid\": %u}}]}";

    SKIP(host);
    
    /* Complete message body, perform JSON RPC call, and check call status */
    sprintf(body, bodyTpl, diag, typ, status, shotnr, datum, uid, pid);
    
    result = execRequest_getResponse(body, response);
    return result;
}

/* GET */
int diagsyncprog_2 (char *host, char *diag, int sleepTime, MonitorInfo * mi)
{
    char body[MAX_REQUEST_SIZE];
    char response[MAX_RESPONSE_SIZE];
    int result;
    char bodyTpl[] =
        "{\"id\": 1, \"jsonrpc\": \"2.0\", \"method\": \"diagsyncRequest\", "
        "\"params\": [{\"__jsonclass__\": [\"ds_defs.DiagSyncRequest\", []], "
	    "\"cmd\": \"monitor\", \"data\": {\"diag\": \"%s\", \"sleep\": %d}}]}";

    SKIP(host);
    
    /* Complete message body, perform JSON RPC call, and check call status */
    sprintf(body, bodyTpl, diag, sleepTime);

    result = execRequest_getResponse(body, response);

    if (result != NO_ERROR) { return result; }

    strcpy(mi->diag, diag);
    /* if (false == getStrValueOf("diag"    response, mi->diag))      return EXIT_FAILURE; */
    if (false == getIntValueOf("typ",    response, &(mi->typ)))    return EXIT_FAILURE;
    if (false == getIntValueOf("status", response, &(mi->status))) return EXIT_FAILURE;
    if (false == getIntValueOf("shotnr", response, &(mi->shotnr))) return EXIT_FAILURE;
    if (false == getIntValueOf("datum",  response, &(mi->datum)))  return EXIT_FAILURE;
    if (false == getIntValueOf("uid",    response, &(mi->uid)))    return EXIT_FAILURE;
    if (false == getIntValueOf("pid",    response, &(mi->pid)))    return EXIT_FAILURE;

    return result;
}

/* SYNC */
int diagsyncprog_3 (char *host, char *diag, int typ, int status, uint32_t shotnr,
		            int datum, uint32_t uid, int pid)
{
    SKIP(host);
    SKIP(diag);
    SKIP(typ);
    SKIP(status);
    SKIP(shotnr);
    SKIP(datum);
    SKIP(uid);
    SKIP(pid);
    return EXIT_SUCCESS;
}

/* RESTART/START/STOP */
int ds_reStartStop (char * cmd, char *diag)
{
    char body[MAX_REQUEST_SIZE];
    char response[MAX_RESPONSE_SIZE];
    int result;
    char bodyTpl[] =
        "{\"id\": 1, \"jsonrpc\": \"2.0\", \"method\": \"diagsyncRequest\", "
        "\"params\": [{\"__jsonclass__\": [\"ds_defs.DiagSyncRequest\", []], "
	    "\"cmd\": \"%s\", \"data\": {\"diag\": \"%s\"}}]}";

    /* Complete message body, perform JSON RPC call, and check call status */
    sprintf(body, bodyTpl, cmd, diag);
    
    result = execRequest_getResponse(body, response);
    return result;
}

int diagsyncprog_4 (char *host, char *diag)
{
    SKIP(host);
    return ds_reStartStop("restart", diag);
}

int diagsyncprog_5 (char *host, char *diag)
{
    SKIP(host);
    return ds_reStartStop("start", diag);
}

int diagsyncprog_6 (char *host, char *diag)
{
    SKIP(host);
    return ds_reStartStop("stop", diag);
}

/* MODE */
int diagsyncprog_7 (char *host, char *diag, char *mode)
{
    char body[MAX_REQUEST_SIZE];
    char response[MAX_RESPONSE_SIZE];
    int result;
    char bodyTpl[] =
        "{\"id\": 1, \"jsonrpc\": \"2.0\", \"method\": \"diagsyncRequest\", "
        "\"params\": [{\"__jsonclass__\": [\"ds_defs.DiagSyncRequest\", []], "
	    "\"cmd\": \"mode\", \"data\": {\"diag\": \"%s\", \"mode\": %s}}]}";

    SKIP(host);
    
    /* Complete message body, perform JSON RPC call, and check call status */
    sprintf(body, bodyTpl, diag, mode);

    result = execRequest_getResponse(body, response);
    return result;
}

/*========== NEW INTERFACE ==========*/

int dsSetStatus(char *host, char *diag, int exptyp, uint32_t shotnr, 
                int status, uint32_t uid, int pid)
{
    return diagsyncprog_1(host, diag, exptyp, status, shotnr, 
                          getDatum(), uid, pid);
}

int dsGetStatus(char *diag, int sleepTime, MonitorInfo * mi)
{
    return diagsyncprog_2(NULL, diag, sleepTime, mi);
}

int dsSynchronize(char *diag)
{
    SKIP(diag);
    return EXIT_SUCCESS;
}

int dsRestart(char *diag)
{
    return ds_reStartStop("restart", diag);
}

int dsStart(char *diag)
{
    return ds_reStartStop("start", diag);
}

int dsStop(char *diag)
{
    return ds_reStartStop("stop", diag);
}

int dsSetMode(char *diag, char *mode)
{
    return diagsyncprog_7(NULL, diag, mode);
}

int dsPutError(char *diag)
{
    SKIP(diag);
    return EXIT_SUCCESS;
}

int dsGetError(char *diag)
{
    SKIP(diag);
    return EXIT_SUCCESS;
}

int dsSendShotFile(char *diag)
{
    SKIP(diag);
    return EXIT_SUCCESS;
}

int dsCleanup(char *diag)
{
    SKIP(diag);
    return EXIT_SUCCESS;
}
