/******************************************************************************
 * diagsync_if.h - Interface for DiagSync server
 *
 * Author: J. C. Gonzalez <jose.carlos.gonzalez@ipp.mpg.de>
 * Last update: 2023.10.13
 ******************************************************************************/

#ifndef DIAGSYNC_IF
#define DIAGSYNC_IF

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "rpcjson.h"

typedef struct {
    char diag[4];
    int typ;
    int status;
    int shotnr;
    int datum;
    int uid;
    int pid;
} MonitorInfo;

void setDiagSyncServerHostPort(char * address, int port);

int diagsyncprog_1 (char *host, char *diag, int typ, int status, uint32_t shotnr,
		            int datum, uint32_t uid, int pid);
int diagsyncprog_2 (char *host, char *diag, int sleepTime, MonitorInfo * mi);
int diagsyncprog_3 (char *host, char *diag, int typ, int status, uint32_t shotnr,
		            int datum, uint32_t uid, int pid);
int diagsyncprog_4 (char *host, char *diag);
int diagsyncprog_5 (char *host, char *diag);
int diagsyncprog_6 (char *host, char *diag);
int diagsyncprog_7 (char *host, char *diag, char *mode);


/* New interface functions */
int dsSetStatus(char *host, char *diag, int exptyp, uint32_t shotnr, 
                int status, uint32_t uid, int pid);
int dsGetStatus(char *diag, int sleepTime, MonitorInfo * mi);
int dsSynchronize(char *diag);
int dsRestart(char *diag);
int dsStart(char *diag);
int dsStop(char *diag);
int dsSetMode(char *diag, char *mode);
int dsPutError(char *diag);
int dsGetError(char *diag);
int dsSendShotFile(char *diag);
int dsCleanup(char *diag);

#ifdef __cplusplus
}
#endif

#endif /* DIAGSYNC_IF */
