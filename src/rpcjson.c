/******************************************************************************
 * rpcjson.h - Implementation of interface to perform calls to the Python RPC 
 *             JSON server
 *
 * Author: J. C. Gonzalez <jose.carlos.gonzalez@ipp.mpg.de>
 * Last update: 2023.07.31
 ******************************************************************************/

#include <stdio.h>              /* printf, sprintf */
#include <stdlib.h>             /* exit, malloc, free */
#include <unistd.h>             /* read, write, close */
#include <sys/socket.h>         /* socket, connect */
#include <netinet/in.h>         /* struct sockaddr_in, struct sockaddr */
#include <netdb.h>              /* struct hostent, gethostbyname */

#include "rpcjson.h"

#define PATH    "/"
#define METHOD  "POST"
#define HEADER1 "Content-Type: application/json"

static bool debug = true;  /* set to true to get debug messages in stderr */

static char serverHostName[128] = HOST;
static int  serverPortNumber    = PORT;

void setServerAddress(char * address, int port)
{
    /* Set host name and port number for server */
    strcpy(serverHostName, address);
    serverPortNumber = port;
}

int execRPCjson(char * body, char * response)
{
    /* first where are we going to send it? */
    char host[128] = HOST;
    int portno = PORT;

    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total, message_size;
    char *message;

    /* Get current server address and port */
    strcpy(host, serverHostName);
    portno = serverPortNumber;
    
    /* How big is the message? */
    message_size = 0;
    message_size += strlen("%s %s HTTP/1.0\r\n");
    message_size += strlen(METHOD);                                  /* method         */
    message_size += strlen(PATH);                                    /* path           */
    message_size += strlen(HEADER1) + strlen("\r\n");                /* header1 */
    message_size += strlen("Content-Length: %lu\r\n") + 20;          /* content length */
    message_size += strlen("\r\n");                                  /* blank line     */
    message_size += strlen(body);                                    /* body           */

    /* allocate space for the message */
    message = malloc(message_size);

    /* fill in the parameters */
    sprintf(message, "%s %s HTTP/1.0\r\n", METHOD, PATH);            /* method, path  */
    strcat(message, HEADER1 "\r\n");                                 /* header1 */
    sprintf(message + strlen(message),
           "Content-Length: %lu\r\n", strlen(body));                 /* content length */
    strcat(message, "\r\n");                                         /* blank line     */
    strcat(message, body);                                           /* body           */

    /* What are we going to send? */
    if (debug) fprintf(stderr, "Request:\n%s\n", message);

    /* create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) return( -SOCKET_ERROR );

    /* lookup the ip address */
    server = gethostbyname(host);
    if (server == NULL) return( -NO_HOST );

    /* fill in the structure */
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr, server->h_addr, server->h_length);

    /* connect the socket */
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        return( -CONNECTION_ERROR );

    /* send the request */
    total = strlen(message);
    sent = 0;
    do {
        bytes = write(sockfd, message + sent, total - sent);
        if (bytes < 0) return( -ERROR_DURING_REQUEST );
        if (bytes == 0) break;
        sent += bytes;
    } while (sent < total);

    free(message);

    /* receive the response */
    total = MAX_RESPONSE_SIZE - 1;
    fprintf(stderr, "total: %d\n", total);
    received = 0;
    do {
        bytes = read(sockfd, response + received, total - received);
        if (bytes < 0) return( -ERROR_DURING_RESPONSE );
        if (bytes == 0) break;
        fprintf(stderr, "received: %d\n", bytes);
        received += bytes;
    } while (received < total);
    close(sockfd);

    /*
     * if the number of received bytes is the total size of the
     * array then we have run out of space to store the response
     * and it hasn't all arrived yet - so that's a bad thing
     */
    if (received == total) return( -CANNOT_STORE_RESPONSE );

    if (debug) printf("Response:\n%s\n\n", response);

    return NO_ERROR;
}
