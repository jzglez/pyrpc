/******************************************************************************
 * get_last_shotno.c - Implementation of the function getLastShotno
 *
 * This functons requests to the Python rshotd server the last shot number,
 * and returns the corresponding shot number, or a code in case of error.
 *
 * Author: J. C. Gonzalez <jose.carlos.gonzalez@ipp.mpg.de>
 * Last update: 2023.10.13
 ******************************************************************************/

#include <stdio.h>              /* printf, sprintf */
#include <stdlib.h>             /* exit, malloc, free */

#include "get_last_shotno.h"

#define RSHOST    "localhost"
#define RSPORT    52016

#define LAST_SHOT_NUMBER    2

static char serverRshotdHostName[128] = RSHOST;
static int  serverRshotdPortNumber    = RSPORT;

static const char * shotnoError[] = {
    "Operation successful",
    "ERROR - Opening socket",
    "ERROR - No such host",
    "ERROR - Connecting to server",
    "ERROR - Writing message to socket",
    "ERROR - Reading response from socket",
    "ERROR - Storing complete response from socket",
    "ERROR - Not valid response from server"
    "ERROR - Cannot find a valid shot number in the response",
};
static char * addError = NULL;
static char fullError[256];

static bool debug = true;  /* set to TRUE to get debug messages in stderr */

void setRShotdServerHostPort(char * address, int port)
{
    /* Set host name and port number for server */
    strcpy(serverRshotdHostName, address);
    serverRshotdPortNumber = port;
}

const char * getShotnoError(shotno_error erno)
{
    if (debug) fprintf(stderr, "ErrCode: %d\n", erno);
    if (addError == NULL) {
        return shotnoError[erno];
    } else {
        sprintf(fullError, "%s (%s)", shotnoError[erno], addError);
        if (debug) fprintf(stderr, "%d ====> %s\n", erno, fullError);
        return (const char *)(fullError);
    }
}

int getLastShotno()
{
    char body[2048];
    char * response;
    char *spos = NULL, *spos1 = NULL;
    int shotno = -1;
    int result;
    
    char bodyTpl[] =
        "{\"id\": 1, \"jsonrpc\": \"2.0\", \"method\": \"rshotRequest\", "
        "\"params\": [{\"__jsonclass__\": [\"rshotd_defs.ShotRequest\", "
        "[%d, 1, \"%s\", \"AUGD\", \"rshotd\", %lu, %d, %u, %u]]}]}";

    
    /* Reset additional error code */
    if (addError != NULL) free(addError);
    addError = NULL;
	
    /* Complete message body, perform JSON RPC call, and check call status */
    sprintf(body, bodyTpl, LAST_SHOT_NUMBER, HOST, getuid(), -99, getpid(), 0);
    response = (char *)malloc(MAX_RESPONSE_SIZE);
    memset(response, 0, MAX_RESPONSE_SIZE);

    setServerAddress(serverRshotdHostName, serverRshotdPortNumber);
    result = execRPCjson(body, response);
    if (result != NO_ERROR) { return result; }

    if (debug) fprintf(stderr, "Response:\n<<<%s>>>\n", response);

    /* Process response */
    spos = strstr(response, "\"shotno\": ");
    spos = strstr(spos+1, "\"shotno\": ");
    if (spos == NULL) {
        spos = strstr(response, "\"response\": ");
        if (spos == NULL) return( -BAD_RESPONSE );
        spos1 = spos;
        spos = strstr(spos1, "]");
        if (spos == NULL) return( -BAD_RESPONSE );
        addError = strndup(spos1, spos - spos1 + 1);
        return( -NOT_VALID_SHOTNO );
    }
    if (debug) fprintf(stderr, "Shotno:\n<<<%s>>>\n", spos + 10);

    sscanf(spos + 10, "%d", &shotno);
    if (shotno < 0) return( -NOT_VALID_SHOTNO );
    
    if (debug) fprintf(stderr, "Shot Number received : %u\n", shotno);

    return( shotno );
}
