/******************************************************************************
 * rpcjson.h - Interface to perform calls to the Python RPC JSON server
 *
 * Author: J. C. Gonzalez <jose.carlos.gonzalez@ipp.mpg.de>
 * Last update: 2023.07.31
 ******************************************************************************/

#ifndef RPCJSON_SERVER
#define RPCJSON_SERVER

#define HOST    "localhost"
#define PORT    52016

#endif /* RPCJSON_SERVER */
