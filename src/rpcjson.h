/******************************************************************************
 * rpcjson.h - Interface to perform calls to the Python RPC JSON server
 *
 * Author: J. C. Gonzalez <jose.carlos.gonzalez@ipp.mpg.de>
 * Last update: 2023.07.31
 ******************************************************************************/

#ifndef RPCJSON
#define RPCJSON

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>

#include "rpcjson_server.h"

typedef enum {
    NO_ERROR,
    SOCKET_ERROR,
    NO_HOST,
    CONNECTION_ERROR,
    ERROR_DURING_REQUEST,
    ERROR_DURING_RESPONSE,
    CANNOT_STORE_RESPONSE,
    BAD_RESPONSE
} shotno_error;

#define MAX_RESPONSE_SIZE 4096

void setServerAddress(char * address, int port);
int execRPCjson(char * body, char * response);
    
#ifdef __cplusplus
}
#endif

#endif /* RPCJSON */
